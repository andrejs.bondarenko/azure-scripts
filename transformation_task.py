import os
from simulation_py_poc.simulation.simulation import add_attribute, drop_attribute
from argparse import ArgumentParser
import json

"""
This task script transforms passed MxG Rules Simulator Export Case Object (JSON) file
and applies specific transformations. Namely it drops three ATTRIBUTES and adds them
with hardcoded data.
"""

def map_case_json(case_json: dict):
    case_life_drivers_licence_number_attribute_json = {
        "id": None,
        "sensitive": False,  # what else can we say :)
        "name": 'DriversLicenseNumber',
        "attributeDefinitionUuid": "eb4e30f1-a2cc-4db5-95fd-6eeb06f261a1",
        "attributeValues": [
            {
                "disclosedValue": None,
                "questionUuid": None,
                "source": 'DISCLOSED',
                "value": {
                    "emptyValue": None,
                    "referenceDataValues": [],
                    "booleanValue": None,
                    "dateValue": None,
                    "datePartialValue": None,
                    "stringValue": "1234567890",  # dummy value
                    "decimalValue": None,
                    "intValue": None,
                }
            }
        ]
    }
    case_life_drivers_licence_number_attribute_path = 'case.life'

    case_life_dob_json = {
        "id": None,
        "sensitive": False,  # what else can we say :)
        "name": 'DateOfBirth',
        "attributeDefinitionUuid": "83467a72-ae8d-4303-b9a5-2dad3e879c68",
        "attributeValues": [
            {
                "disclosedValue": None,
                "questionUuid": None,
                "source": 'DISCLOSED',
                "value": {
                    "emptyValue": None,
                    "referenceDataValues": [],
                    "booleanValue": None,
                    "dateValue": "1980-01-01",
                    "datePartialValue": None,
                    "stringValue": None,
                    "decimalValue": None,
                    "intValue": None,
                }
            }
        ]
    }
    case_life_dob_attribute_path = 'case.life'

    case_life_mvr_dob_json = {
        "id": None,
        "sensitive": False,  # what else can we say :)
        "name": 'DateOfBirth',
        "attributeDefinitionUuid": "b3d6c583-d2cd-468d-a9cd-c6e997473ea2",
        "attributeValues": [
            {
                "disclosedValue": None,
                "questionUuid": None,
                "source": 'DISCLOSED',
                "value": {
                    "emptyValue": None,
                    "referenceDataValues": [],
                    "booleanValue": None,
                    "dateValue": "1980-01-01",
                    "datePartialValue": None,
                    "stringValue": None,
                    "decimalValue": None,
                    "intValue": None,
                }
            }
        ]
    }
    case_life_mvr_dob_attribute_path = 'case.life.MotorVehicleRecords.Record'

    attributes_jsons = [case_life_drivers_licence_number_attribute_json,
                        case_life_dob_json,
                        case_life_mvr_dob_json]
    attributes_paths = [case_life_drivers_licence_number_attribute_path,
                        case_life_dob_attribute_path,
                        case_life_mvr_dob_attribute_path]

    json_path_pairs = zip(attributes_jsons, attributes_paths)

    dropped_attributes = []
    drop_attribute(case_json=case_json,
                   dropped_attributes=dropped_attributes,
                   attribute_locator='case.life',
                   attribute_name='DriversLicenseNumber')
    drivers_licence_number_dropped = len(dropped_attributes) > 0

    drop_attribute(case_json=case_json,
                   dropped_attributes=[],
                   attribute_locator='case.life',
                   attribute_name='DateOfBirth')

    for _json_to_add, _path_to_add_jspon in json_path_pairs:
        # we will not add DriversLicenceNumber for cases where it was absent (had None value due to sensitivity flag)
        if drivers_licence_number_dropped or not _json_to_add['name'] == 'DriversLicenseNumber':
            add_attribute(case_json=case_json, attribute_locator=_path_to_add_jspon, attribute_value=_json_to_add)
        else:
            print('Skipped adding DriversLicenseNumber as it was originally absent.')

def ensure_folder_exists(output_filepath: str):
    """
    Function cheks file path, if folders for storing such file
    are absent - entire folders structure is created.
    :param output_filepath: file path to check and ensure folders exist
    :return: None (folders structure is created if needed for supplied filepath)
    """
    folder_path = os.path.dirname(output_filepath)
    if not folder_path is None and len(folder_path) > 0:
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

def read_json(file):
    f = open(file, 'r', encoding='utf-8')
    _json = f.read()
    f.close()
    return _json


def write_json(json_dict: dict, file_name_out: str):
    print('write_json...')
    if file_name_out and json_dict:
        print('Writing results to file:' + file_name_out)
        ensure_folder_exists(file_name_out)
        print('Preparing to write file...')
        with open(file_name_out, 'w') as f:
            print('Writing file...')
            json.dump(json_dict, f)
            print('DONE.')
    else:
        raise Exception('Both file_name_out = '+file_name_out + '; and json_dict = ' + str(json_dict) +'should be defined!')


def main(file_name_in: str, file_name_out: str):
    print('file_name_in=' + file_name_in)
    print('file_name_out=' + file_name_out)

    json_str = read_json(file_name_in)
    json_dict = json.loads(json_str)

    print('Transforming json...')

    map_case_json(json_dict)

    print('Transformation done...')

    write_json(json_dict, file_name_out)

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-f_in', '--file_in', dest='file_name_in', help='JSON file to be read')
    parser.add_argument('-f_out', '--file_out', dest='file_name_out', help='JSON file to be written')
    args = parser.parse_args()
    file_name_in = args.file_name_in
    file_name_out = args.file_name_out

    main(file_name_in, file_name_out)
